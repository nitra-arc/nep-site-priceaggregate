PriceAggregateBundle
==============

Enable the bundle
=================

Enable the bundle in the kernel:

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Nitra\PriceAggregateBundle\NitraPriceAggregateBundle(),
        // ...
    );
}
```