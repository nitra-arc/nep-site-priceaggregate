<?php

namespace Nitra\PriceAggregateBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(collection="PriceAggregators")
 */
class PriceAggregator
{
    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Entry name
     * @ODM\String
     */
    protected $name;

    /**
     * @var string Result file name
     * @ODM\String
     */
    protected $fileName;

    /**
     * @var \Nitra\ProductBundle\Document\Category[] Ignored categories
     * @ODM\ReferenceMany(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $categories;

    /**
     * @var integer Price from filter for products
     * @ODM\Int
     */
    protected $priceFrom;

    /**
     * @var integer Price to filter for products
     * @ODM\Int
     */
    protected $priceTo;

    /**
     * @var string Main template for render xml
     * @ODM\String
     */
    protected $mainTemplate;

    /**
     * @var string Template for render categories
     * @ODM\String
     */
    protected $categoryTemplate;

    /**
     * @var string Template for render products
     * @ODM\String
     */
    protected $productTemplate;

    /**
     * @var string Template for render product parameters
     * @ODM\String
     */
    protected $productParametersTemplate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fileName
     * @param string $fileName
     * @return self
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * Get fileName
     * @return string $fileName
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Add category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function addCategory(\Nitra\ProductBundle\Document\Category $category)
    {
        $this->categories[] = $category;
        return $this;
    }

    /**
     * Remove category
     * @param \Nitra\ProductBundle\Document\Category[] $category
     * @return self
     */
    public function removeCategory(\Nitra\ProductBundle\Document\Category $category)
    {
        $this->categories->removeElement($category);
        return $this;
    }

    /**
     * Get categories
     * @return \Nitra\ProductBundle\Document\Category[] $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set priceFrom
     * @param int $priceFrom
     * @return self
     */
    public function setPriceFrom($priceFrom)
    {
        $this->priceFrom = $priceFrom;
        return $this;
    }

    /**
     * Get priceFrom
     * @return int $priceFrom
     */
    public function getPriceFrom()
    {
        return $this->priceFrom;
    }

    /**
     * Set priceTo
     * @param int $priceTo
     * @return self
     */
    public function setPriceTo($priceTo)
    {
        $this->priceTo = $priceTo;
        return $this;
    }

    /**
     * Get priceTo
     * @return int $priceTo
     */
    public function getPriceTo()
    {
        return $this->priceTo;
    }

    /**
     * Set mainTemplate
     * @param string $mainTemplate
     * @return self
     */
    public function setMainTemplate($mainTemplate)
    {
        $this->mainTemplate = $mainTemplate;
        return $this;
    }

    /**
     * Get mainTemplate
     * @return string $mainTemplate
     */
    public function getMainTemplate()
    {
        return $this->mainTemplate;
    }

    /**
     * Set categoryTemplate
     * @param string $categoryTemplate
     * @return self
     */
    public function setCategoryTemplate($categoryTemplate)
    {
        $this->categoryTemplate = $categoryTemplate;
        return $this;
    }

    /**
     * Get categoryTemplate
     * @return string $categoryTemplate
     */
    public function getCategoryTemplate()
    {
        return $this->categoryTemplate;
    }

    /**
     * Set productTemplate
     * @param string $productTemplate
     * @return self
     */
    public function setProductTemplate($productTemplate)
    {
        $this->productTemplate = $productTemplate;
        return $this;
    }

    /**
     * Get productTemplate
     * @return string $productTemplate
     */
    public function getProductTemplate()
    {
        return $this->productTemplate;
    }

    /**
     * Set productParametersTemplate
     * @param string $productParametersTemplate
     * @return self
     */
    public function setProductParametersTemplate($productParametersTemplate)
    {
        $this->productParametersTemplate = $productParametersTemplate;
        return $this;
    }

    /**
     * Get productParametersTemplate
     * @return string $productParametersTemplate
     */
    public function getProductParametersTemplate()
    {
        return $this->productParametersTemplate;
    }
}