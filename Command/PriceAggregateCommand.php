<?php

namespace Nitra\PriceAggregateBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Routing\RouterInterface;
use Nitra\StoreBundle\Lib\Globals;

class PriceAggregateCommand extends NitraContainerAwareCommand
{
    /** @var \Symfony\Component\Console\Input\InputInterface */
    protected $input;

    /** @var \Symfony\Component\Console\Output\OutputInterface */
    protected $output;

    /** @var \Nitra\StoreBundle\Document\Store */
    protected $store;

    /** @var \Liip\ImagineBundle\Imagine\Cache\CacheManager */
    protected $cacheManager;

    /** @var \Twig_Environment */
    protected $twig;

    /** @var \Symfony\Component\Routing\RouterInterface */
    protected $router;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('nitra:price-aggregator:generate')
            ->setDescription('generate price aggregate files')
            ->addOption('name', null, InputOption::VALUE_OPTIONAL, 'Name of price aggregators', '')
            ->addOption('host', null, InputOption::VALUE_OPTIONAL, 'Store host', 'localhost');
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->input    = $input;
        $this->output   = $output;

        $this->cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        $this->twig         = $this->getContainer()->get('nitra.twig.string');
        $this->router       = $this->getContainer()->get('router');

        $this->initStore();
    }

    /**
     * Initialize store
     */
    protected function initStore()
    {
        $host = $this->input->getOption('host');
        $this->store = $this->getDocumentManager()->getRepository('NitraStoreBundle:Store')->findOneByHost($host);

        if (!$this->store) {
            throw new NotFoundResourceException(sprintf("Store by host \"%s\" not found", $host));
        }

        $storeInfo = $this->formatStoreArray($this->store);

        Globals::$container = $this->getContainer();
        Globals::setStore($storeInfo);

        $this->router->getContext()->setHost($host);
    }

    /**
     * Formatting store to array
     *
     * @param \Nitra\StoreBundle\Document\Store $store  Store instance
     *
     * @return array
     */
    protected function formatStoreArray($store)
    {
        return array(
            'id'              => $store->getId(),
            'host'            => $store->getHost(),
            'discountBadgeId' => $store->getDiscountBadge()
                ? $store->getDiscountBadge()->getId()
                : null,
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $aggregators = $this->getAggregators();

        foreach ($aggregators as $aggregator) {
            $this->processAggregator($aggregator);
        }
    }

    /**
     * @return \Nitra\PriceAggregateBundle\Document\PriceAggregator[]
     */
    protected function getAggregators()
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraPriceAggregateBundle:PriceAggregator');

        if ($name = $this->input->getOption('name')) {
            $qb->field('name')->equals($name);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * Processing aggregator
     *
     * @param \Nitra\PriceAggregateBundle\Document\PriceAggregator $aggregator
     */
    protected function processAggregator($aggregator)
    {
        list ($categories, $ignoreCategoriesIds) = $this->processAggregatorCategories($aggregator);
        $products   = $this->processAggregatorProducts($aggregator, $ignoreCategoriesIds);

        $catCont    = $this->renderCategories($aggregator->getCategoryTemplate(), $categories);
        $prodCont   = $this->renderProducts($aggregator->getProductTemplate(), $aggregator->getProductParametersTemplate(), $products);
        $content    = $this->renderContent($aggregator->getMainTemplate(), $catCont, $prodCont);
        
        // Открываем файл в режиме записи 
        $fileName   = $this->getContainer()->get('kernel')->getRootDir() . '/../web/' . $aggregator->getFileName() . '.xml';
        $pointer    = fopen($fileName, "w");
        // Write message to output
        $this->output->writeln('Aggregator "' .
            $this->format($aggregator->getName(), self::STYLE_BOLD, self::COLOR_RED) .
            '" ' .
            (fwrite($pointer, $content) ? "updated successfully" : "not updated")
        );
        // Закрытие файла
        fclose($pointer);
    }

    /**
     * Render template
     *
     * @param string $template
     * @param array  $categories
     * @param array  $products
     *
     * @return string   Html
     */
    protected function renderContent($template, $categories, $products)
    {
        return $this->twig->render(preg_replace(array('/{{\s*products\s*}}/', '/{{\s*categories\s*}}/'), array('{{ products|raw }}', '{{ categories|raw }}'), $template), array(
            'storeName'     => $this->store->getName(),
            'storeHost'     => $this->store->getHost(),
            'date'          => date("Y-m-d H:m"),
            'categories'    => $categories,
            'products'      => $products,
        ));
    }

    /**
     * Render categories
     *
     * @param string $template
     * @param array  $categories
     *
     * @return null|string
     */
    protected function renderCategories($template, $categories)
    {
        $content = null;
        foreach ($categories as $id => $category) {
            $content .= $this->twig->render($template, array(
                'id'            => $id,
                'category'      => $category,
            ));
        }

        return $content;
    }

    /**
     * Render products
     *
     * @param string $template
     * @param string $parametersTemplate
     * @param array  $products
     *
     * @return null|string
     */
    protected function renderProducts($template, $parametersTemplate, $products)
    {
        $content = null;
        foreach ($products as $id => $product) {
            $content .= $this->twig->render(preg_replace('/{{\s*parameters\s*}}/', '{{ parameters|raw }}', $template), array(
                'id'            => $id,
                'product'       => $product,
                'parameters'    => $this->renderParameters($parametersTemplate, $product['parameters']),
            ));
        }

        return $content;
    }

    /**
     * Render parameters for product
     *
     * @param string $template
     * @param array  $parameters
     *
     * @return null|string
     */
    protected function renderParameters($template, $parameters)
    {
        $content = null;
        foreach ($parameters as $name => $value) {
            $content .= $this->twig->render($template, array(
                'name'  => $name,
                'value' => $value,
            ));
        }

        return $content;
    }

    /**
     * Processing categories for aggregator
     *
     * @param \Nitra\PriceAggregateBundle\Document\PriceAggregator $aggregator
     *
     * @return array
     */
    protected function processAggregatorCategories($aggregator)
    {
        if (count($aggregator->getCategories())) {
            $ignorePaths = array();
            foreach ($aggregator->getCategories() as $category) {
                $ignorePaths[] = preg_quote($category->getPath());
            }

            $ignoreCategoriesIds = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
                ->distinct('_id')
                ->field('path')->equals(new \MongoRegex('/^' . implode('|', $ignorePaths) . '/i'))
                ->getQuery()->execute()->toArray();
        } else {
            $ignoreCategoriesIds = array();
        }

        $categories = $this->getCategories($ignoreCategoriesIds);

        $result = array();
        foreach ($categories as $category) {
            $id = crc32($category->getId());

            $result[$id] = array(
                'name'      => $category->getName(),
                'parent'    => $category->getParent() ? crc32($category->getParent()->getId()) : null,
            );
        }

        return array($result, $ignoreCategoriesIds, );
    }

    /**
     * Get active categories
     *
     * @param array $ignore
     *
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getCategories($ignore)
    {
        return $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->field('isActive')->equals(true)
            ->field('id')->notIn($ignore)
            ->getQuery()->execute();
    }

    /**
     * Processing products for aggregator
     *
     * @param \Nitra\PriceAggregateBundle\Document\PriceAggregator $aggregator
     * @param array $ignoreCategoriesIds
     *
     * @return array
     */
    protected function processAggregatorProducts($aggregator, $ignoreCategoriesIds)
    {
        $products = array();
        foreach ($this->getRowProducts($aggregator, $ignoreCategoriesIds) as $product) {
            $id = crc32($product->getId());
            $products[$id] = array();
            $products[$id]['parameters']    = $this->formatProductParameters($product);
            $products[$id]['article']       = $product->getArticle();
            $products[$id]['name']          = $product->getFullName();
            $products[$id]['vendor']        = $product->getModel()->getBrand();
            // 512 symbols max
            $products[$id]['description']   = $product->getDescription()
                ? substr(strip_tags($product->getDescription()), 0, 512)
                : null;
            $products[$id]['categoryId']    = crc32($product->getModel()->getCategory()->getId());
            $products[$id]['categoryName']  = htmlspecialchars($product->getModel()->getCategory()->getSingularName());
            $products[$id]['price']         = $product->getPrice(false);
            $products[$id]['picture']       = $product->getImage()
                ? $this->cacheManager->getBrowserPath($product->getImage(), 'big_thumb', true)
                : null;
            $products[$id]['url']           = $this->router->generate('product_page', array(
                'slug'  => $product->getAliasEn(),
            ), RouterInterface::ABSOLUTE_URL);
        }

        return $products;
    }

    /**
     * Get products for aggregator
     *
     * @param \Nitra\PriceAggregateBundle\Document\PriceAggregator $aggregator
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    protected function getRowProducts($aggregator, $ignoreCategoriesIds)
    {
        $qb = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('category.id')->notIn($ignoreCategoriesIds);

        $storePriceField = 'storePrice.' . $this->store->getId() . '.price';

        if (!is_null($aggregator->getPriceFrom())) {
            $qb->field($storePriceField)->gte((float) $aggregator->getPriceFrom());
        }

        if (!is_null($aggregator->getPriceTo())) {
            $qb->field($storePriceField)->lte((float) $aggregator->getPriceTo());
        }

        return $qb->getQuery()->execute();
    }

    /**
     * Format product parameters
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return array
     */
    protected function formatProductParameters($product)
    {
        $parameters = array();
        if ($product->getParameters()) {
            foreach ($product->getParameters() as $parameter) {
                if ($parameter->getValues()) {
                    $values = array();
                    foreach ($parameter->getValues() as $value) {
                        $values[] = $value->getName();
                    }
                    $parameters[$parameter->getParameter()->getName()] = implode(', ', $values);
                }
            }
        }

        return $parameters;
    }
}